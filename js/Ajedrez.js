var Ajedrez = (function() {
  var tableroDiv = document.getElementById("tablero");
  var opcionDiv = document.getElementById("opcion");
  var mensaje = document.getElementById("mensaje");

  var _llenar_Tabla = function(documento_csv) {
    if (tableroDiv.firstChild) {
      tableroDiv.removeChild(tableroDiv.firstChild);
    }
    var xml_nodos = documento_csv.length;
    var tablero = document.createElement("table");
    for (var i = 0; i < 9; i++) {
      var tabla_tr = document.createElement("tr");
      var contador_td = document.createElement("td");
      if (i===0) {
        contador_td.textContent = "";
      } else {
        contador_td.textContent = (9-i);
      }
      tabla_tr.appendChild(contador_td);
      for (var j=0; j < 8; j++) {//campos.length; j++) {
        var tabla_td = document.createElement("td");
        tabla_td.textContent = documento_csv.charAt((j*2)+(i*16));
        tabla_tr.appendChild(tabla_td);
      }
      tablero.appendChild(tabla_tr);
    }
    tableroDiv.appendChild(tablero);
  };

  var _verificar_caracter = function(caracter) {
    if (caracter === "♜") {
      return true;
    } else if (caracter === "♞") {
      return true;
    } else if (caracter === "♝") {
      return true;
    } else if (caracter === "♛") {
      return true;
    } else if (caracter === "♚") {
      return true;
    } else if (caracter === "♟") {
      return true;
    } else if (caracter === "∅") {
      return true;
    } else if (caracter === "♙") {
      return true;
    } else if (caracter === "♖") {
      return true;
    } else if (caracter === "♘") {
      return true;
    } else if (caracter === "♗") {
      return true;
    } else if (caracter === "♕") {
      return true;
    } else if (caracter === "♔") {
      return true;
    }
    return false;
  }

  var _verificar_csv = function(contenido_csv) {
    //Validando longitud total
    if (contenido_csv.length < 142) {
      return false;
    }
    //Validando que tenga 9 filas (8 saltos de línea)
    for (var i = 0; i < 8; i++) {
      if (contenido_csv.charAt(15+(16*i)) !== "\n") {
        return false;
      }
    }
    //Validando caracteres en el tablero
    for (var i = 1; i < 9; i++) {
      for (var j = 0; j < 8; j++) {
        if (!_verificar_caracter(contenido_csv.charAt((j*2)+(i*16)))) {
          return false;
        }
      }
    }
    return true;
  };

  var _descargar = function() {
    var _rutaCSV = "csv/tablero.csv";
    var servidor = window.location.toString();//"http://localhost:8080/";
    var url = servidor + _rutaCSV;
    var xhr = new XMLHttpRequest();
    mensaje.textContent = "Actualizando...";
    xhr.onreadystatechange = function() {
      if (this.readyState === 4) {
        if (this.status === 200) {
          if (_verificar_csv(this.responseText)) {
            _llenar_Tabla(this.responseText);
            mensaje.textContent = "Listo";
          } else {
            mensaje.textContent = ("Tablero no válido");
          }
        } else {
          mensaje.textContent = ("Error " + this.status + " " + this.statusText + ", no se pudo obtener el archivo " + this.responseURL);
        }
      }
    };
    xhr.open('GET', url, true);
    xhr.send();
  };

  var _letras_a_numeros = function(letra){
    if (letra==="a"){
      return 1;
    } else if (letra==="b"){
      return 2;
    } else if (letra==="c"){
      return 3;
    } else if (letra==="d"){
      return 4;
    } else if (letra==="e"){
      return 5;
    } else if (letra==="f"){
      return 6;
    } else if (letra==="g"){
      return 7;
    } else if (letra==="h"){
      return 8;
    }
  };

  var _es_valido = function(numero) {
    if (numero==="1"){
      return true;
    } else if (numero==="2"){
      return true;
    } else if (numero==="3"){
      return true;
    } else if (numero==="4"){
      return true;
    } else if (numero==="5"){
      return true;
    } else if (numero==="6"){
      return true;
    } else if (numero==="7"){
      return true;
    } else if (numero==="8"){
      return true;
    }
    return false;
  };

  var _mostrar_tablero = function() {
    //var tablero = document.createElement("table");
    var boton = document.createElement("button");
    //Rellenando el tablero
    _descargar();
    //Atributos
    boton.textContent = "Actualizar Tablero";
    boton.addEventListener("click", _actualizar_tablero);
    //tableroDiv.appendChild(tablero);
  	opcionDiv.appendChild(boton);
  };
  var _actualizar_tablero= function() {
    _descargar();
  };
  var _mover_pieza = function(obj_parametros) {
    if (tableroDiv.firstChild) {
      if (obj_parametros.de && obj_parametros.a) {
        var de_0 = obj_parametros.de.charAt(0);
        var de_1 = obj_parametros.de.charAt(1);
        var a_0 = obj_parametros.a.charAt(0);
        var a_1 = obj_parametros.a.charAt(1);
        if (_letras_a_numeros(de_0) && _letras_a_numeros(a_0) && _es_valido(de_1) && _es_valido(a_1)) {
          var tabla = tableroDiv.firstChild;
          var tr_de = tabla.rows.item(9 - de_1);
          var td_de = tr_de.cells.item(_letras_a_numeros(de_0));
          var tr_a = tabla.rows.item(9 - a_1);
          var td_a = tr_a.cells.item(_letras_a_numeros(a_0));
          //Intercambio
          if (td_de.textContent !== "∅") {
            if (td_a.textContent === "∅") {
              var aux = td_de.textContent;
              td_de.textContent = td_a.textContent;
              td_a.textContent = aux;
              mensaje.textContent = "Listo";
            } else {
              mensaje.textContent = "El lugar está ocupado por otra pieza";
            }
          } else {
            mensaje.textContent = "La pieza a mover no existe";
          }
        } else {
          mensaje.textContent = "Movimiento no válido";
        }
      } else {
        mensaje.textContent = "Parámetro no válido";
      }
    }
  };
  return {
    "mostrarTablero": _mostrar_tablero,
    "actualizarTablero": _actualizar_tablero,
    "moverPieza": _mover_pieza
  }
})();
